package com.lentra.ssl.ltpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;


@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
public class LtplApiApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(LtplApiApplication.class, args);
	}

}
