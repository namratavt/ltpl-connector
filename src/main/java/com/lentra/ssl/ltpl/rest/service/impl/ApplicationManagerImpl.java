package com.lentra.ssl.ltpl.rest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lentra.ssl.ltpl.configuration.LtplConfig;
import com.lentra.ssl.ltpl.rest.service.ApplicationManager;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationManagerImpl implements ApplicationManager {

	private static Logger logger = LoggerFactory.getLogger(ApplicationManagerImpl.class);
	
	@Autowired
	LtplConfig ltplConfig;

	@Override
	public String getLtplResponse(String ltplRequest) {

		Client client = Client.create();
		try {
			final String url = ltplConfig.getUrl();
			System.out.println(url);
			WebResource webResource = client.resource(url);
			String response = webResource.entity(ltplRequest,"application/json").post(String.class);

			return response;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			client = null;
			ltplRequest = null;
		}

	}
}
