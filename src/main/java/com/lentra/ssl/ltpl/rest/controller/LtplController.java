package com.lentra.ssl.ltpl.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lentra.ssl.ltpl.rest.service.impl.ApplicationManagerImpl;

@RestController
public class LtplController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ApplicationManagerImpl applicationManagerImpl;

	@RequestMapping(value = "/getResponse", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public String getLtplResponse(@RequestBody String request) {

		logger.info("Inside Ltpl_Controller >> getLtplResponse");
		logger.info("Ltpl Request:" + request);

		return applicationManagerImpl.getLtplResponse(request);
	}

}
